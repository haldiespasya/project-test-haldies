
import React, { useState, useEffect } from 'react';

import Logo from './_components/logo';

import { Menu, X } from 'lucide-react';
import Navbar from './_components/navbar';


const Header = () => {
    const [isHeaderVisible, setIsHeaderVisible] = useState(true);
    const [lastScrollPosition, setLastScrollPosition] = useState(0);
    const [isMobileMenuOpen, setIsMobileMenuOpen] = useState(false);

    const toggleMobileMenu = () => {
        setIsMobileMenuOpen(!isMobileMenuOpen);
    };

    const handleScroll = () => {
        const scrollPosition = window.scrollY;
        console.log(scrollPosition);
        setIsHeaderVisible(scrollPosition < lastScrollPosition || scrollPosition < 50);
        setLastScrollPosition(scrollPosition);
    };

    useEffect(() => {
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, [lastScrollPosition]);

    return (
        <header className={`flex z-50 top-0 left-0 w-full justify-between bg-[#FF6600] ${isHeaderVisible ? 'fixed translate-y-0 opacity-95' : 'fixed -translate-y-20 opacity-0'} transition-all duration-700 ease-in-out`}>
            <div className="pl-5 lg:pl-32">
                <Logo />
            </div>
            <nav className="hidden md:flex justify-end items-center md:pr-20 lg:pr-48 lg:flex sm:hidden">
                <Navbar />
            </nav>
            <div className="relative pr-6  flex items-center w-1/2 justify-end lg:hidden md:hidden">
                <div className={`transition-all duration-300 absolute z-50  ${isMobileMenuOpen ? 'transform rotate-45' : 'transform rotate-0'}`}>
                    {isMobileMenuOpen ? (
                        <X
                            className="text-white cursor-pointer"
                            size={40}
                            onClick={toggleMobileMenu}
                        />
                    ) : (
                        <Menu
                            className="text-white cursor-pointer"
                            size={40}
                            onClick={toggleMobileMenu}
                        />
                    )}
                </div>

            </div>

            {isMobileMenuOpen && (
                <div className="bg-[#934816] p-4 w-full absolute h-screen">
                    <div className="mobileNavbar flex flex-col w-full items-center mt-28 h-1/2 justify-around">
                        <Navbar />
                    </div>
                </div>
            )}
        </header >
    );
};

export default Header;
