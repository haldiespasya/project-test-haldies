import { Link, useLocation } from 'react-router-dom';


const Navbar = () => {
    const { pathname } = useLocation(); // Get the current pathname from the route
    return (
        <>

            <Link to="/" className={pathname === '/' ? 'active' : ''}>
                Home
            </Link>
            <Link to="/services" className={pathname === '/services' ? 'active' : ''}>
                Services
            </Link>
            <Link to="/about" className={pathname === '/about' ? 'active' : ''}>
                About
            </Link>
            <Link to="/ideas" className={pathname === '/ideas' ? 'active' : ''}>
                Ideas
            </Link>
            <Link to="/careers" className={pathname === '/careers' ? 'active' : ''}>
                Careers
            </Link>
            <Link to="/contact" className={pathname === '/contact' ? 'active' : ''}>
                Contact
            </Link>
        </>
    );
}

export default Navbar;