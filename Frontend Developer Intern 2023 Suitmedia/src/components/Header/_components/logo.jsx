

const Logo = () => {
    return ( 
        <>
        <div className="w-32 p-4">
            <img src="https://suitmedia.com/_nuxt/img/logo-white.081d3ce.png" alt="suitmedia.png" />
        </div>
        </>
     );
}
 
export default Logo;