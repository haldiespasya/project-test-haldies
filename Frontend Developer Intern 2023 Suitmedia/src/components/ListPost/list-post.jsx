import React, { useState, useEffect } from "react";
import axios from "axios";
import { useNavigate, useLocation } from "react-router-dom";
import Loading from "../Loading/loading";
import {
    ChevronLeft,
    ChevronRight,
    ChevronsLeft,
    ChevronsRight
} from "lucide-react";
const API_URL = "https://suitmedia-backend.suitdev.com/api/ideas";

const ListPost = () => {
    const navigate = useNavigate();
    const location = useLocation();
    const queryParams = new URLSearchParams(location.search);
    const currentPageParam = queryParams.get("page") || 1;
    const itemsPerPageParam = queryParams.get("size") || 10;
    const sortOptionParam = queryParams.get("sort") || "published_at";

    const [posts, setPosts] = useState([]);
    const [currentPage, setCurrentPage] = useState(Number(currentPageParam));
    const [itemsPerPage, setItemsPerPage] = useState(Number(itemsPerPageParam));
    const [totalPages, setTotalPages] = useState(1);
    const [lastPage, setLastPage] = useState(1);
    const [sortOption, setSortOption] = useState(sortOptionParam);
    const [loading, setLoading] = useState(true);


    useEffect(() => {
        // Fungsi untuk memuat data dari API
        const cache = {};
        const fetchData = async () => {
            try {
                setLoading(true);

                const cachedResponse = cache[API_URL];
                if (cachedResponse) {
                    console.log('Using cached response');
                    setPosts(cachedResponse.posts);
                    setTotalPages(cachedResponse.totalPages);
                    setLoading(false);
                    return;
                }
                const response = await axios.get(API_URL, {
                    params: {
                        "page[number]": currentPage,
                        "page[size]": itemsPerPage,
                        append: ['small_image', 'medium_image'],
                        sort: sortOption,
                    },
                });
                const postsWithFormattedDate = response.data.data.map(post => ({
                    ...post,
                    formattedDate: formatDate(post.published_at),
                }));
                setLastPage(response.data.meta.last_page);
                setPosts(postsWithFormattedDate);
                setTotalPages(response.data.meta.total);
            } catch (error) {
                if (error.response && error.response.status === 429) {
                    console.warn('Rate limit exceeded. Retrying in 5 seconds...');
                    await new Promise(resolve => setTimeout(resolve, 5000));   
                    fetchData();
                    setLoading(true);
                } else {
                    console.error('Error fetching data:', error);
                    setLoading(true);
                }

            } finally {
                setLoading(false); 
            }
        };

        fetchData();
    }, [currentPage, itemsPerPage, sortOption]);
    useEffect(() => {
       
        const params = new URLSearchParams();
        params.set("page", String(currentPage));
        params.set("size", String(itemsPerPage));
        params.set("sort", sortOption);
        navigate(`?${params.toString()}`);
    }, [currentPage, itemsPerPage, sortOption, navigate]);

    const handlePageChange = (newPage) => {
        setCurrentPage(newPage);
    };

    const handleItemsPerPageChange = (newSize) => {
        setItemsPerPage(newSize);
        setCurrentPage(1);
    };
    const handleSortChange = (newSortOption) => {
        setSortOption(newSortOption);
    };

    const formatDate = (originalDate) => {
        const options = { day: 'numeric', month: 'long', year: 'numeric' };
        const formattedDate = new Date(originalDate).toLocaleDateString('id-ID', options);
        return formattedDate;
    };
    const handleImageError = (e) => {
        e.target.src = "fallback_image_url";
    };

    return (
        <main className="flex w-full items-center flex-col ">
            <div className="flex w-11/12 lg:w-3/4 m-4 justify-between h-10 ">
                <p className="font-bold">Showing {((currentPage - 1) * itemsPerPage) + 1} - {Math.min(currentPage * itemsPerPage, totalPages)} of {totalPages} </p>
                <div className="flex">
                    <div className="w-auto  flex  text-center">
                        <select className="pl-2 m-1  border bg-white rounded-md pr-2" value={sortOption} onChange={(e) => handleSortChange(e.target.value)}>
                            <option value="published_at">Terlama</option>
                            <option value="-published_at">Terbaru</option>
                        </select>
                    </div>
                    <div className="w-auto  flex text-center ">
                        <select className="pl-2 border m-1  bg-white rounded-md pr-2" value={itemsPerPage} onChange={(e) => handleItemsPerPageChange(Number(e.target.value))}>
                            <option value={10}>10</option>
                            <option value={20}>20</option>
                            <option value={50}>50</option>
                        </select>
                    </div>
                </div>
            </div>
            <article className="w-11/12 md:11/12 lg:w-3/4  flex flex-wrap justify-center xl:justify-between lg:justify-between sm:justify-center">
                {loading ? (    
                        Array.from({ length: posts.length || itemsPerPage }, (_, index) => (
                            <div key={index} className="w-11/12 sm:w-[280px] sm:m-2 lg:m-0 md:w-[250px] lg:w-[275px] mb-4 rounded-md drop-shadow-md h-auto border">
                                <Loading />
                            </div>
                        ))
                ) : (
                    posts.map(post => (
                        <div key={post.id} className="w-11/12 sm:w-[280px] sm:m-2 md:w-[250px] lg:w-[275px] mb-4 rounded-md drop-shadow-md h-auto border ">
                            {post.small_image && post.small_image[0] && post.small_image[0].url ? (
                                <img
                                    className="w-full h-40 object-cover rounded-sm"
                                    src={post.small_image[0].url}
                                    alt={post.title}
                                    loading="lazy"
                                    onError={handleImageError}
                                />
                            ) : (
                                <div className="h-40 flex justify-center items-center bg-slate-200 w-full"><p>Image not available</p></div>
                            )}
                            <div className="p-4">
                                <p>{post.formattedDate}</p>
                                <h3 className="font-bold overflow-hidden line-clamp-3">{post.title}</h3>
                            </div>
                        </div>
                    ))
                )}
            </article>

            <div className="w-auto m-5 flex text-center">
                <button className="w-auto p-2  rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage - currentPage + 1)} disabled={currentPage === 1}>
                    <ChevronsLeft />
                </button>
                <button className="w-auto p-2  rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage - 1)} disabled={currentPage === 1}>
                    <ChevronLeft />
                </button>
                <button className="w-10 p-2 border rounded-md hover:bg-slate-200 " onClick={() => handlePageChange(currentPage - 1)}>
                    <span>{currentPage}</span>
                </button>
                <button className="w-10 p-2 border rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage + 1)} style={{ display: currentPage === lastPage ? 'none' : 'block' }}>
                    <span>{currentPage + 1}</span>
                </button>
                <button className="w-10 p-2 border rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage + 2)} style={{ display: currentPage === lastPage ? 'none' : 'block' }}>
                    <span>{currentPage + 2}</span>
                </button>
                <button className="w-10 p-2 border rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage + 3)} style={{ display: currentPage === lastPage ? 'none' : 'block' }}>
                    <span>{currentPage + 3}</span>
                </button>
                <button className="w-10 p-2  rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage + 1)} disabled={currentPage === lastPage}>
                    <ChevronRight />
                </button>
                <button className="w-auto p-2  rounded-md hover:bg-slate-200" onClick={() => handlePageChange(currentPage + lastPage - currentPage)} disabled={currentPage === lastPage}>
                    <ChevronsRight />
                </button>
            </div>

        </main>
    );
}
export default ListPost;
