import React, { useState, useEffect } from "react";
import bannerDataJson from "../data.json";

const Banner = () => {
  const [bannerDataState, setBannerDataState] = useState({});
  const [scrollPosition, setScrollPosition] = useState(0);

  useEffect(() => {
    setBannerDataState(bannerDataJson[0]);

    // Update scroll position on scroll
    const handleScroll = () => {
      setScrollPosition(window.scrollY);
    };

    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  const parallaxStyle = {
    transform: `translateY(${scrollPosition * 0.5}px)`, // Adjust the factor for the desired parallax effect
  };

  return (
    <div className="lg:h-96 h-full w-full border bg-slate-200 relative overflow-hidden">
      <div style={parallaxStyle}>
        <img
          className="w-full h-full object-cover"
          src={bannerDataState.imageUrl}
          alt="Banner"
        />
      </div>
      <div className="banner-overlay absolute top-0 left-0 w-full h-full flex items-center justify-center text-white">
        <h1 className="text-4xl font-bold">Your Text Goes Here</h1>
      </div>
      <div className="bg-slate-200 lg:w-[120%] lg:h-56 absolute lg:-right-15 top-40 -right-2 lg:-right-10 w-[110%] h-32 -rotate-6 lg:top-[300px] lg:-rotate-6">
       
      </div>
    </div>
  );
};

export default Banner;
