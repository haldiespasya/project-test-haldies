import React from "react";

const Footer = () => {
  const currentYear = new Date().getFullYear();

  return (
    <footer className=" bg-[#e96106] text-white p-4 text-center ">
      <p>&copy; {currentYear} Haldies Gerhardien Pasya. All rights reserved.</p>
    </footer>
  );
};

export default Footer;
