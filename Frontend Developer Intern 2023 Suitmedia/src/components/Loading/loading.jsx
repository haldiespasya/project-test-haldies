
import React from 'react';

const Loading = ({ containerHeight, containerWidth }) => {
  return (
    <div className="border-blue-300">
      <div className="animate-pulse">
        <div className="bg-slate-200 w-full h-40"></div>
        <div className="flex-1 space-y-5 p-4">
          <div className="h-2 bg-slate-200 rounded w-32"></div>
          <div className="space-y-3 ">
            <div className="h-2 bg-slate-200 rounded w-48"></div>
            <div className="h-2 bg-slate-200 rounded"></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Loading;
