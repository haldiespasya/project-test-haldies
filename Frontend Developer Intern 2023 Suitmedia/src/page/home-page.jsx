import ListPost from "../components/ListPost/list-post";
import Banner from "../components/banner";
import Header from "../components/Header/header";
import Footer from "../components/Footer";



const Home = () => {
    return (
        <>
            <Header />
            <div className="pt-16">
                <Banner />
            </div>
            <ListPost />
            <Footer />
        </>
    );
}

export default Home;