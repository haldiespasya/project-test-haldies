
import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import Home from './page/home-page';
import ErrorPage from './page/404';
import ServicesPage from './page/services-page';
import IdeasPage from './page/ideas-page';
import CareersPage from './page/careers-page';
import ContactPage from './page/contact-page';
import AbourPage from './page/about-page';


const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="*" element={<ErrorPage/>} />
        <Route path="/about" element={<AbourPage />} />
        <Route path="/services" element={<ServicesPage/>} />
        <Route path="/idea" element={<IdeasPage />} />
        <Route path="/careers" element={<CareersPage/>} />
        <Route path="/contact" element={<ContactPage />} />
      </Routes>
    </Router>
  );
};

export default App;
